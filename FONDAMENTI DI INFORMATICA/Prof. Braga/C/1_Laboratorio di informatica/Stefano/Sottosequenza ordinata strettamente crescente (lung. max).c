#include <stdio.h>

int main()
{
	int sen=1, last=0, lung=0, lmax=0;
	printf("----Sottosequenza ordinata strettamente crescente (lung. max)----\n");
	printf("Inserisci una sequenza di numeri (interi positivi) e terminala inserendo zero: ");
	scanf("%d", &sen);
	while(sen!=0){
		if(sen>last){
			last=sen;
			lung++;
			scanf("%d", &sen);
			if(lmax<lung)
				lmax=lung;
		}
		else{
			last=sen;
			lung=1;
			scanf("%d", &sen);
		}
	}
	if(lung!=0){
		printf("Lung. max = %d", lmax);
	}
	else{
		printf("Non hai inserito nessun numero");
	}
	
	return 0;
}
