//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX 10000

int main() {
    
    FILE * fp;
    int num_1,num_2;
    //char stringa [MAX];
    //char nome_file [MAX], nuovo_nome [MAX];
    
    //fp = fopen("/Users/roberto/Documents/Prove_TXT/prova1.txt", "r");             //apro file in modilità read prova1
    
    fp = fopen("/Users/roberto/Documents/Prove_TXT/prova2.txt", "w");               //apro file in modalità write prova2
    
    if(fp != NULL)                                                                  //controllo apertura file
        
        printf("Successo apertura file\n\n");                                       //andato a buon fine
    
    else
        
        printf("errore apertura file\n\n");                                         //errore apertura file
    
    num_1 = 120;                                                                    //assegnamento dell'int per la fprintf(...)
    fprintf(fp, "\n%d\n", num_1);                                                   //scrittura di un int su file
    
    /*while (1) {                                                                   //lettura intero file testo
        
        if(fgets(stringa,sizeof(stringa),fp) == NULL)
            break;                                                                  //esce quando ha terminato l'intero file di testo
        
        printf("%s",stringa);                                                       //stampa in output intero file testo letto
        
    }*/                                                                             //fine lettura intero file
    
    printf("\n\n");                                                                 //stampa spazi per estetica
    
    printf("%d\n\n",feof(fp));                                                      //verifica errore con feof(fp) == 0 <-> if fp!=NULL
     
    /*fgets(stringa, sizeof(stringa), fp);                                          //lettura stringa nel file
    printf("%s\n", stringa);*/                                                      //stampa in output stringa letta
    
    /*fscanf(fp, "%d%d",&num_1,&num_2);                                             //lettura numeri nel file
    printf("num1: %d\nnum2: %d\n", num_1,num_2);*/                                  //stampa in output numeri letti
    
    /*printf("metti nome file: ");                                                  //inizio funzione rename
    scanf("%s", nome_file);
    
    printf("metti nome nuovo: ");
    scanf("%s",nuovo_nome);
    
    if(rename(nome_file, nuovo_nome) == 0)
        printf("successful\n");
    else
        printf("error\n");*/                                                        //fine funzione rename
    
    /*if(remove("/Users/roberto/Documents/Prove_TXT/prova2.txt")==0)                //inizio rimozione file
        
        printf("file cancellato\n");
    
    else
        
        printf("errore nella cancellazione del file\n");*/                          //fine rimozione file
    
    fclose(fp);


    return 0;

}
