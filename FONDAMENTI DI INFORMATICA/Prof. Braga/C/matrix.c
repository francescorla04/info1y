#include <stdio.h>

#define sep 79

int main(void) {
	
	int n,k;
	
	scanf("%d%d", &n,&k);
	
	printf("   ");
	
	for(int i = 1; i<=k; i++){
		
		printf("%4d ",i);
		
	}
	
	printf("\n");
	
	for (int i = 0; i < sep; i++) {
		
		printf("-");
		
	}
	
	printf("\n");
	
	for(int i = 1; i<=n; i++){
		
		printf("%d: ", i);
		
		for(int j = 1; j<=k; j++)
			printf("%4d ",i*j);
		
		printf("\n");
		
	}
	
}